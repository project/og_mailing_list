<?php

/**
* @file
* Contains mail formatting
*
*/

function og_mailing_list_mailbox_load($node) {
  $mailbox = array(
  'type' => 'imap',
  'folder' => 'INBOX',
  'domain' => 'imap.gmail.com',
  'encrypted_connection' => 'ssl',
  'port' => '993',
  // imap settings
  'name' => $node->og_mailbox_username[LANGUAGE_NONE][0]['value'],
  'pass' =>  password_field_decrypt($node->og_mailbox_password[LANGUAGE_NONE][0]['password_field']),
  // smtp settings
  'smtp_server' => $node->og_mailbox_smtp_server[LANGUAGE_NONE][0]['value'],
  'smtp_server_port' => $node->og_mailbox_smtp_port[LANGUAGE_NONE][0]['value'],
  'smtp_username' => $node->og_mailbox_smtp_username[LANGUAGE_NONE][0]['value'],
  'smtp_password' =>  password_field_decrypt($node->og_mailbox_smtp_password[LANGUAGE_NONE][0]['password_field']),
  'extraimap' => '/ssl/novalidate-cert',
  'limit' => '0',
  'encoding' => 'UTF-8',
  'flag_after_read' => 1,
  'delete_after_read' => 0,
  'fromheader' => 'From',
  'security' => '0',
  'replies' => '0',
  'mime' => 'TEXT/HTML,TEXT/PLAIN',
  'node' => $node,
  );
  return $mailbox;
}


// TODO: add arguments
function og_mailing_list_comment_process_message($header, $origbody, $mailbox, $mimeparts, $mimebody) {
  $from =  reset($header->from);

  $member_mail = $from->mailbox . '@' . $from->host;
  // only retrieve enabled users (not blocked)
  $enable_members_mails = og_mailing_list_enabled_members_array($mailbox['node']->og_members);

  // verify if mail belongs to group
  if (in_array($member_mail, array_keys($enable_members_mails))) {
    $user = user_load($enable_members_mails[$member_mail]);
    // verify if user has permission
    if (user_access('retrieve and send all messages for mailing list', $user) || user_access('publish comments for mailing list', $user)) {
      // we must process before authenticating because the password may be in Commands
      $comment = og_mailing_list_comment_prepare_message($header, $origbody, $mimebody, $mailbox, $enable_members_mails[$member_mail]);
      // Put $mimeparts on the comment
      $comment->mimeparts = $mimeparts;

      og_mailing_list_comment_submit($comment, $header, $mailbox, $origbody);
    }
  }

}


/**
 * Append default commands. Separate commands from body. Strip signature.
 * Return a node object.
 */
function og_mailing_list_comment_prepare_message($header, $body, $mimebody, $mailbox, $uid) {
  // Initialise a node object
  $comment = new stdClass();

  // Copy any name/value pairs from In-Reply-To or References e-mail headers to $node. Useful for maintaining threading info.
  if (!empty($header->references)) {
    // we want the final element in references header, watching out for white space
    $threading = substr(strrchr($header->references, '<'), 0);
  }
  elseif (!empty($header->in_reply_to)) {
    $threading = str_replace(strstr($header->in_reply_to, '>'), '>', $header->in_reply_to); // Some MUAs send more info in that header.
  }
  if (isset($threading) && $threading = rtrim(ltrim($threading, '<'), '>')) { //strip angle brackets
    if ($threading) $comment->threading = $threading;
  }

  // decode encoded subject line
  $subjectarr = imap_mime_header_decode($header->subject);
  if (empty($subjectarr)) {
    $comment->subject = truncate_utf8(trim(decode_entities(strip_tags(check_markup($body)))), 29, TRUE);
  }
  else {
    for ($i = 0; $i < count($subjectarr); $i++) {
      if ($subjectarr[$i]->charset != 'default')
      $comment->subject .= drupal_convert_to_utf8($subjectarr[$i]->text, $subjectarr[$i]->charset);
      else
      $comment->subject .= $subjectarr[$i]->text;
    }
  }

  $comment->comment_body[LANGUAGE_NONE][0]['value'] = $body;
  $comment->comment_body[LANGUAGE_NONE][0]['format'] = og_mailing_list_set_filter_format($mimebody);
  $comment->created = $header->udate;
  $comment->status = COMMENT_PUBLISHED;
  $comment->language = LANGUAGE_NONE;
  $comment->is_anonymous = 0;
  $comment->uid = $uid;
  $params = og_mailing_list_parse_messageparams($comment->threading);

  // second email
  if (isset($comment->threading) && ($params['pid'] == 0)) {
    $comment->pid = $params['cid'];
  }
  // more mails
  elseif (isset($comment->threading) && ($params['pid'] !== 0)) {
    $comment->pid = $params['pid'];
  }
  // first email
  else {
    $comment->pid = 0;
  }

  if (isset($comment->threading) && ($params['nid'] !== 0)) {
    $comment->nid = $params['nid'];
  }

  if (!isset($params['nid']) && !isset($params['pid'])) {
    $group_nid = $mailbox['node']->nid;
    $filter = og_mailing_list_filter_subject($comment->subject, $group_nid);
    if ($filter) {
      $comment->nid = $filter->entity_id;
    }
    else {
      $comment->nid = $group_nid;
    }
    $comment->pid = 0;
  }
  return $comment;
}

function og_mailing_list_filter_subject($subject, $group_nid) {
  $query = db_select('field_data_og_mailbox_filter', 't');
  $query->join('og_membership', 'o', 't.entity_id = o.etid');
  $query->fields('t', array('entity_id', 'og_mailbox_filter_value'));
  $query->condition('o.gid', $group_nid);
  $filters = $query->execute()->fetchAllAssoc('entity_id');
  foreach ($filters as $filter) {
    $token = str_replace(array(' ', '.'), array('\s', '\.') , $filter->og_mailbox_filter_value);
    if (preg_match("/\b{$token}\b/i", $subject)) {
      return $filter;
    }
  }
  return FALSE;
}

/**
 * Create the comment.
 */
function og_mailing_list_comment_submit($comment, $header, $mailbox, $origbody) {
  $node = node_load($comment->nid);
  $field_info_instance = field_info_instance('comment', OG_COMMENT_FILES_FIELDNAME, 'comment_node_' . $node->type);
  $settings = $field_info_instance['settings'];

  $comment->body = $origbody;

  foreach ($comment->mimeparts as $mimepart) {
    if ($mimepart->filename !== 'unnamed_attachment') {
      base64_decode($mimepart->data);
      $file = file_save_data($mimepart->data, file_default_scheme() . '://' . $settings['file_directory'] . '/' . $mimepart->filename);
      if ($file == FALSE) {
        throw new Exception("attachment can not be saved, maybe destination directory is not properly configured");
      }
      $file->uid = $comment->uid;
      $file->display = 1;
      $file->description = $mimepart->filename;
      $comment->field_files[LANGUAGE_NONE][] = (array)$file;
    }
  }

  unset($comment->threading);
  unset($comment->mimeparts);
  unset($comment->mimebody);
  unset($comment->body);


  // Prepare a comment for submission.
  comment_submit($comment);

  // Accepts a submission of new or changed comment content.
  comment_save($comment);
}

function og_mailing_list_set_filter_format($mimebody) {
  $mimetype = array(
    'TEXT/PLAIN' => 'plain_text',
    'TEXT/HTML'  => 'full_html',
  );
  return $mimetype[$mimebody];
}

function og_mailing_list_set_filter_mimebody($format) {
  $mimebody = array(
    'plain_text' => 'text/plain',
    'full_html'  => 'text/html',
  );
  return $mimebody[$format];
}

function og_mailing_list_parse_messageparams($messageid) {
  $params = array();
  $parts = explode('@', $messageid);
  if (count($parts) == 2 && $parts[1] == variable_get('og_mailing_list_server_name', 'localhost')) {
    $parts = explode('.', $parts[0]);
    if (count($parts) == 6) {
      $params['pid'] = $parts[0];
      $params['uid'] = $parts[1];
      $params['nid'] = $parts[2];
      $params['cid'] = $parts[3];
      $params['time'] = $parts[4];
      $params['signature'] = $parts[5];
    }
  }
  return $params;
}
