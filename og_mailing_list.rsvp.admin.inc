<?php

/**
 * @file
 * Admin settings for RSVP
 */

module_load_include('inc', 'og_mailing_list', 'og_mailing_list.batch');

/**
 * RVSP admin form
 * @param  [type] $form        [description]
 * @param  [type] $form_state  [description]
 * @param  [type] $entity_type [description]
 * @param  [type] $nid         [description]
 * @return [type]              [description]
 */
function og_ui_admin_rsvp_form($form, $form_state, $entity_type, $nid) {

  $form = array();

  $form['emails'] = array(
    '#title' => 'E-mails',
    '#description' => t('Please, enter e-mails, one by line'),
    '#type' => 'textarea',
    '#rows' => 10,
  );

  $form['message'] = array(
    '#title' => 'Message',
    '#description' => t('Please, enter your welcome message'),
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#rows' => 10,
  );

  $form['demo'] = array(
    '#title' => 'Send demo message',
    '#description' => t('Send a demo message to mails listed above'),
    '#type' => 'checkbox',
  );

  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $nid,
  );

  $form['entity_type'] = array(
    '#type' => 'hidden',
    '#value' => $entity_type,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function og_ui_admin_rsvp_form_submit($form, $form_state) {
  $emails = explode(PHP_EOL, $form_state['values']['emails']);
  $demo = $form_state['values']['demo'];
  $message = $form_state['values']['message']['value'];

  $group_nid = $form_state['values']['nid'];
  // @todo validate e-mail
  $i = 0;
  foreach ($emails as $email) {
    $i++;
    $email = trim($email);
    // validate if is member of this group
    $uid = $demo == 1 ? FALSE: og_mailing_list_validate_member($group_nid, $email);

    if (filter_var($email, FILTER_VALIDATE_EMAIL) && $uid == FALSE) {
       // validate if user exists
       $uid = og_mailing_list_validate_user($email);
       if ($uid) {
         $new = FALSE;

       }
       else {
         $new = TRUE;
       }
       $operations[] = array('og_mailing_list_send_rsvp', array($group_nid, $uid, $email, $message, $new, $i, t('(Email @email)', array('@email' => $email))));
    }
  }
  if (!empty($operations)) {
    $batch = array(
      'operations' => $operations,
      'finished' => 'og_mailing_list_finished',
    );
    batch_set($batch);
    return drupal_set_message(t('process completed'));
  }
  else {
    return drupal_set_message(t('nothing to do, all emails are already registered in this forum'));
  }
}

/**
 * Function batch process
 * @param  [type] $group_nid         [description]
 * @param  [type] $uid               [description]
 * @param  [type] $email             [description]
 * @param  [type] $new               [description]
 * @param  [type] $i                 [description]
 * @param  [type] $operation_details [description]
 * @param  [type] $context           [description]
 * @return [type]                    [description]
 */
function og_mailing_list_send_rsvp($group_nid, $uid, $email, $message, $new, $i, $operation_details, &$context) {
  $gnode = node_load($group_nid);
  $mailbox = og_mailing_list_mailbox_load($gnode);
  $params = array(
    'key' => 'og_mailing_list',
    'headers' => array(
      'Return-Path' => $mailbox['name'],
      'X-Mailer' => 'Drupal',
      'time' => time(),
    ),
    'to' => trim($email),
  );
  $params['body_format'] = 'full_html';

  if ($new == TRUE) {
    $params['subject'] = t('Forum @forum [Instructions to register and participate]', array('@forum' => $gnode->title));
    $params['body'] = og_mailing_list_register_message($gnode, $uid, $email, $message);
  }
  else {
    $params['subject'] = t('Forum @forum [Instructions to log in and participate]', array('@forum' => $gnode->title));
    $params['body'] = og_mailing_list_login_message($gnode, $uid, $email, $message);
  }

  $send = og_mailing_list_sendmail($mailbox, $params);

  if ($send) {
    watchdog('og_mailing_list', 'Forum ' . $gnode->title . ': mail sent to ' . $email, array(), WATCHDOG_NOTICE);
  }
  else {
    watchdog('og_mailing_list', 'Forum ' . $gnode->title . ': mail did not send to ' . $email, array(), WATCHDOG_NOTICE);
  }

  $context['results'][] = $i;
  // Optional message displayed under the progressbar.
  $context['message'] = t('Sending mail @id', array('@id' => $i)) . ' ' . $operation_details;
  og_mailing_list_update_http_requests();

}

/**
 * Prepare message to register in when you are new user
 * @param  [type] $gnode [description]
 * @param  [type] $uid   [description]
 * @param  [type] $email [description]
 * @return [type]        [description]
 */
function og_mailing_list_register_message($gnode, $uid, $email, $message) {
  global $base_url;
  $body = t("Dear") . ' ' . $email . '<br />';
  $body .= $message . '<br />';
  $body .= t("Please, create an account <a href='{$base_url}/user/register'>here</a>") . '<br />';
  $body .= t("Next, go to <a href='{$base_url}/user/login?destination=group/node/@gid/subscribe/og_user_node' >here</a> to participate in Forum @forum", array('@gid' => $gnode->nid, '@forum' => $gnode->title)) . '<br />';
  $body .= t("The Editor team") . '<br />';
  return $body;
}

/**
 * Prepare message to log in when you are old user
 * @param  [type] $gnode [description]
 * @param  [type] $uid   [description]
 * @param  [type] $email [description]
 * @return [type]        [description]
 */
function og_mailing_list_login_message($gnode, $uid, $email, $message) {
  global $base_url;
  $body  = t("Dear") . ' ' . $email . '<br />';
  $body .= $message . '<br />';
  $body .= t("Please, log in into your <a href={$base_url}/user>account</a> to participate in Forum @forum", array($uid, '@forum' => $gnode->title)) . '.<br />';
  $body .= t("After, go to <a href='{$base_url}/group/node/@gid/subscribe/og_user_node' > link</a> to register.", array('@gid' => $gnode->nid)) . '<br />';
  $body .= t("The Editor team") . '<br />';
  return $body;
}


/**
 * Return uid given group nid and email
 * @param  [type] $group_nid [description]
 * @param  [type] $email     [description]
 * @return [type]            [description]
 */
function og_mailing_list_validate_member($group_nid, $email) {
  $query = db_select('users', 'u');
  $query->join('og_membership', 'o', 'o.etid = u.uid');
  $query->fields('o', array('etid'));
  $query->condition('u.mail', $email);
  $query->condition('o.entity_type', 'user');
  $query->condition('o.gid', $group_nid);
  $uid = $query->execute()->fetchField(0);
  return $uid;
}

/**
 * Validate if user exists
 * @param  [type] $email [description]
 * @return [type]        [description]
 */
function og_mailing_list_validate_user($email) {
  $query = db_select('users', 'u');
  $query->fields('u', array('uid'));
  $query->condition('u.mail', $email);
  $uid = $query->execute()->fetchField(0);
  return $uid;
}
