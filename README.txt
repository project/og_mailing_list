1) Instructions for 7.x-1.x branch

Setup

a) Go to admin/config/group/fields and add 3 fields called "Group mailbox username", "Group mailbox password" and "Group mailbox enable" to your bundle settings (OG content type). See https://bitbucket.org/asanchez75/og_mailing_list/downloads/v1_og_mailing_list_settings_1.png
b) Add a node (OG content type) and fill out the username (Group mailbox username) and password (Group mailbox password) of your Gmail account you will use as group account. See https://bitbucket.org/asanchez75/og_mailing_list/downloads/v1_og_mailing_list_settings_2.png
c) To retrieve mails click in button "Retrieve messages". See https://bitbucket.org/asanchez75/og_mailing_list/downloads/v1_og_mailing_list.png
d) If you want to post comments with attachments please create a file field called "files" in your comment field settings. See https://bitbucket.org/asanchez75/og_mailing_list/downloads/v1_og_mailing_list_settings_3.png

The images are available here https://bitbucket.org/asanchez75/og_mailing_list/downloads

Notes

a) You need to setup manually a Gmail account as group account for each node of your OG content type you have created
b) You can include attachments in your post comments :)
c) Enable IMAP Access in your Gmail account
d) A better option is the module OG Mailinglist (http://drupal.org/project/og_mailinglist, without space) but it requires root access on your site's server and fairly advanced sysadmin skills.
e) The library to retrieve mails was adapted from mailhandler-6.x-1.x (http://drupal.org/project/mailhandler)
f) Video tutorial (https://www.youtube.com/watch?v=u6GVakP2tjQ)


2) Instructions for 7.x-2.x branch

a) Go to admin/config/group/fields and add 3 fields called "Group mailbox username", "Group mailbox password" and "Group mailbox enable" to your OG node group type. See https://drupal.org/files/project-images/group1.png
b) Go to admin/config/group/fields and add 1 field called "Group mailbox filter" to your OG node content type. See https://drupal.org/files/project-images/group4.png
c) Add a OG group node and and fill out the username (Group mailbox username) and password (Group mailbox password) of your Gmail account you will use as group account. See https://drupal.org/files/project-images/group2.png
d) To retrieve mails click in button "Retrieve messages".
e) If you want to post comments with attachments please create a file field called "files" in your comment field settings. See https://bitbucket.org/asanchez75/og_mailing_list/downloads/v1_og_mailing_list_settings_3.png
f) For each node (OG content type) you have already added, you have to add a string to filter mail subject. See https://drupal.org/files/project-images/group3.png
g) Include RVSP to send invitations if users still are not registered in this group. See https://drupal.org/files/project-images/group5.png

This module is sponsored by CONDESAN
